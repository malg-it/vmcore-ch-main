#!/bin/bash
export VM_ROOT="${VM_ROOT-/opt/vmcore}"
export VM_LOCAL="${VM_LOCAL-$HOME/.vm}"
source "$VM_ROOT/lib/xsh-lite.lib.sh"
source "$VM_ROOT/lib/dialog.lib.sh"
source "$VM_ROOT/config.inc.sh"
[ -r "$VM_LOCAL/profile.inc.sh" ] && source "$VM_LOCAL/profile.inc.sh"

F_FIX1=
F_FIX2=
F_FORCE1=
F_FORCE2=
# Arguments parsing
usage() {
    echo "Stampa l'utilizzo delle risorse."
    echo ""
    echo " -f|--fix:        Tenta la risoluzione dei problemi"
    echo " -f1|--fix-ram:   Tenta la risoluzione dei problemi RAM"
    echo " -f2|--fix-vdi:   Tenta la risoluzione dei problemi VDI"
    echo " -F[1|2]|--force: Forza le azioni correttive"
}
while [ "$#" -gt 0 ]; do
    case "$1" in
        --fix|-f)
            F_FIX1=1
            F_FIX2=1
            ;;
        --fix-ram|-f1)
            F_FIX1=1
            ;;
        --fix-vdi|-f2)
            F_FIX2=1
            ;;
        --force|-F)
            F_FIX1=1
            F_FIX2=1
            F_FORCE1=1
            F_FORCE2=1
            ;;
        --force-fix-ram|-F1)
            F_FIX1=1
            F_FORCE1=1
            ;;
        --force-fix-vdi|-F2)
            F_FIX2=1
            F_FORCE2=1
            ;;
        --help|"-?")
            usage >&2
            exit helper
            ;;           
    esac
    shift
done

## RAM
# Total RAM > 2GB
ram_t="$(($(free|grep ^Mem|awk '{print $2}')/1024))"
if [ "$ram_t" -lt 2048 ]; then
    wlog "W: La RAM ($ram_t MiB) dev'essere incrementata manualmente.\n   CTRL + click: ${__f_RGB}file:///home/dev/MEGA/vm/docs/content/t_moreram.html${__z}"
else
    wlog "K: RAM totale allocata : $ram_t MiB"
fi
# Free RAM > 512MB
ram_f="$(($(free|grep ^Mem|awk '{print $4}')/1024))"
_ram_free() {
    [ "$F_FIX1" ] || return 1
    wlog " I: Soppressione delle caches..."
    echo 3|sudo tee /proc/sys/vm/drop_caches >/dev/null
    local ram_f2="$(($(free|grep ^Mem|awk '{print $4}')/1024))"
    local ram_f2d="$(($ram_f2-$ram_f))"
    local ram_f2p="$((100-($ram_f*100)/$ram_f2))"
    wlog " K: Recuperati $ram_f2d MiB (-$ram_f2p%)"
}
if [ "$ram_f" -lt 512 ]; then
    wlog "E: RAM libera CRITICA : $ram_f MiB (richiesti >512 MiB)"
    _ram_free
elif [ "$ram_f" -lt 1024 ]; then
    wlog "W: RAM libera carente : $ram_f MiB (suggeriti >1024 MiB)"
    _ram_free
else
    wlog "K: RAM libera ....... : $ram_f MiB"
    [ "$F_FORCE1" ] && _ram_free
fi
# Used SWAP == 0
swap_u="$(($(free|grep ^Swap|awk '{print $3}')/1024))"
if [ "$swap_u" -gt 0 ]; then
    wlog "W: Rilevata attività di swapping ($swap_u MiB, meglio se =0 MiB)."
else
    wlog "K: Swapping non rilevato."
fi

sync
_vdi_part_check() { # </dev/mapper/system-root>
    ddev="$1"
    dname="$2"
    #disk_t="$(($(df|grep "$ddev\$"|awk '{print $2}')/1024))"
    #disk_t="$(($(df|grep "$ddev\$"|awk '{print $2}')/1024))"
    disk_t="$(df|grep "$ddev\$"|awk '{print $2}')/1024)"
    if [ "$disk_t" -lt 0 ]; then #20480
        wlog "E: La partizione %s (%d MiB) dev'essere incrementata.\n   CTRL + click: ${__f_RGB}https://git.cav94mat.com/MALG/vmcore/wiki/FAQ${__z}" "$dname" "$disk_t"
    else
        wlog "I: Spazio totale su '%s' ....... : %d MiB (%d GiB)" "$dname" "$disk_t" "$(($disk_t/1024))"
    fi
    disk_f="$(($(df|grep "$ddev\$"|awk '{print $4}')/1024))"
    disk_fp="$((($disk_f*100)/$disk_t))"
    if [ "$disk_f" -lt 1024 ]; then
        wlog "E: Spazio libero su '%s' CRITICO : %d MiB (%d%%)" "$dname" "$disk_f" "$disk_fp"
        _vdi_part_free
    elif [ "$disk_f" -lt 5120 ]; then
        wlog "W: Spazio libero su '%s' carente : %d MiB (%d%%)" "$dname" "$disk_f" "$disk_fp"
        _vdi_part_free
    else
        wlog "W: Spazio libero su '%s' ....... : %d MiB (%d%%)" "$dname" "$disk_f" "$disk_fp"
        [ "$F_FORCE2" ] && _vdi_part_free
    fi
}
_vdi_part_free() { # </dev/mapper/system-root>
    [ "$F_FIX2" ] || return 1
    ddev="$1"
    wlog " I: Pulizia del disco..."
    sudo bleachbit --preset --clean >/dev/null
    local disk_f2="$(($(df|grep "$ddev\$"|awk '{print $4}')/1024))"
    local disk_f2d="$(($disk_f2-$disk_f))"
    local disk_f2p="$((100-($disk_f*100)/$disk_f2))"
    wlog "K: Recuperati %d MiB (%d%% recuperato)" "$disk_f2d" "$disk_f2p"
    F_FIX2=2
}

_vdi_part_check "/opt" "home"
_vdi_part_check "/home" "root"
